import requests
from bs4 import BeautifulSoup
import urllib.parse
import wget
from PyPDF2 import PdfFileReader

def get_pdfs(my_url):
    links = []
    html = requests.get(my_url)
    html.encoding = html.apparent_encoding
    content = html.content
    html_page = BeautifulSoup(content, features="lxml") 
    og_url = html_page.find("meta",  property = "og:url")
    base = urllib.parse(my_url)
    print("base",base)
    for link in html_page.find_all('a'):
        current_link = link.get('href')
        if current_link.endswith('pdf'):
            if og_url:
                print("currentLink",current_link)
                links.append(og_url["content"] + current_link)
            else:
                links.append(base.scheme + "://" + base.netloc + current_link)

    for link in links:
        try: 
            wget.download(link)
        except:
            print(" \n \n Unable to Download A File \n")

if __name__ == "__main__":
    get_pdfs()