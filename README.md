# WebScraper101

Testes de WebScraping, com o objetivo de baixar automaticamente pdf's dos TRT's

## Primeiros Passos

```
cd repositório_de_escolha
git clone https://gitlab.com/CleberFNeto/webscraper101.git
code . (ou abra a pasta na IDE de sua escolha)
```

## Instalando pré-requisitos

- [baixe o WebDriver do Google Chrome](https://chromedriver.chromium.org/downloads)
> Descubra a versão do seu Google Chrome indo em "Help" (Ajuda) e posteriormente indo em "Detalhes do Navegador"

## Instalando requirements.txt

```
source venv_WebScraping/bin/activate
pip install -r requirements.txt
```

## Detalhes do WebScraper

Após executar o programa, o Web Scraper vai rodar no background, passando por todos os diários (TST e TRT's de todas as 24 Regiões) e baixando seus respectivos diários em PDF, após isso, ele salva os mesmos com seus respectivos códigos na pasta Downloads.

