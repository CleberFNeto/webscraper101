from pathlib import Path
from os.path import getmtime, expanduser
from os import rename

def renomeia_pdf(novo_nome):
    download_folder = expanduser("~")+"/Downloads/"
    diretorio = Path(download_folder)
    pdfs = diretorio.glob('*.pdf')
    new_pdf = max(pdfs, key=getmtime)
    rename("{}".format(new_pdf), download_folder + "{}.pdf".format(novo_nome))

def troca_nome_por_codigo(nome, data):
    nomes = ['TST']
    for x in range (1,25):
        nomes.append('TRT {}ª Região'.format(x))
    codigo = ['55', '456', '461', '92', '83', '71', '150', '12', '11', '146', '51', '457', '458', '154', '182', '66', '158', '67', '22', '194', '2', '162', '174', '170', '37']
    indice = nomes.index(nome)
    nome_codigo = ('{}'.format(codigo[indice]) + data)
    return renomeia_pdf(nome_codigo)