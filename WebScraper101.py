#! /usr/bin/python3

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from rename import troca_nome_por_codigo
from Arquivo_def import diretorio_arquivo
from time import sleep


def opcoes_de_navegacao():

    download_folder = diretorio_arquivo()

    options = Options()
    options.add_argument("--headless")
    diretorio = {"download.default_directory" : "{}".format(download_folder),
                "download.prompt_for_download": False,
                "download.directory_upgrade": True,
                "plugins.always_open_pdf_externally": True,
                "profile.default_content_setting_values.automatic_downloads": 1}
    options.add_experimental_option("prefs",diretorio)
    navegador = webdriver.Chrome(options=options)

    url = 'https://dejt.jt.jus.br/dejt/f/n/diariocon'
    navegador.get(url)

    return navegador

def captura_de_data (navegador):
    data = navegador.find_element_by_id('corpo:formulario:dataIni')
    data = '{}'.format(data.get_attribute('value'))
    data = data.replace('/','')

    return data

def define_caderno():
    id_Caderno = 'corpo:formulario:tipoCaderno'
    Valor_Caderno = '1'

    return id_Caderno, Valor_Caderno

def select_id(navegador, id, valor):
    Seleciona_ = Select(navegador.find_element_by_id(id))
    Seleciona_.select_by_value(valor)

def click_id(navegador, id):
    Clica_Pesquisar = navegador.find_element_by_id(id)
    Clica_Pesquisar.click()

def click_xpath(navegador, id):
    Clica_Pesquisar = navegador.find_element_by_xpath(id)
    Clica_Pesquisar.click()

def todas_regioes(navegador):
    Regiões_aux = navegador.find_element_by_id('corpo:formulario:tribunal')
    Regiões = Regiões_aux.find_elements_by_tag_name('option')

    lista_regiões = Regiões_aux.text
    lista_regiões = lista_regiões.split('\n')

    return Regiões, lista_regiões

def regiao_pass(lista_regiões, Região):
    Região_Nova = lista_regiões[Região+1]
    Regiões_Desnecessarias = ['CSJT','ENAMAT']
    if Região_Nova not in Regiões_Desnecessarias:
        return True, Região_Nova
    else:
        return False, Região_Nova

def regiao_(Regiões, lista_regiões, navegador, data):
    for Região in range (len(Regiões)-1):
        try:
            Pass, Região_Nova = regiao_pass(lista_regiões, Região)
            if Pass:
                select_id(navegador, 'corpo:formulario:tribunal', '{}'.format(Região))
                click_id(navegador, 'corpo:formulario:botaoAcaoPesquisar')
                try:
                    click_xpath(navegador, '//button[@class="bt af_commandButton"]')
                    troca_nome_por_codigo(Região_Nova, data)
                    sleep(6)
                except:
                    print("Download não disponível")
                    sleep(6)
        except:
            print("URL Inválida")

def finalizando_execucao(navegador):
    navegador.close()

if __name__ == "__main__":
    navegador = opcoes_de_navegacao()
    data = captura_de_data(navegador)
    id_Caderno, Valor_Caderno = define_caderno()
    select_id(navegador, id_Caderno, Valor_Caderno)
    Regiões, lista_regiões = todas_regioes(navegador)
    regiao_(Regiões, lista_regiões, navegador, data)
    finalizando_execucao(navegador)